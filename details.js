const apiUrl = 'https://api.myjson.com/bins/kgegi'

const backButton = () => {
    const backButton = document.getElementById('close-icon')
    backButton.addEventListener('click', function() {
        window.location = '/'
    })
}

const getParams = () => {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    const func = urlParams.get('function')
    return func
}

const displayInfos = (d) => {
    const container = document.createElement('div')
    container.id = 'content-container'
    container.innerHTML = `
    <div class="description-container">
        <ul class="description">
            <li>${d.definition}</li>
        </ul>
    </div>
    <div class="code-container">
        <img src="img/${d.codeImg}" alt="">
        <img src="img/${d.resultat}" alt="">
    </div>`
    document.getElementById('container').append(container)
    document.querySelector('h1').innerHTML = `${d.nom}() method`
}

const getFunctions = async() => {
    let response = await fetch(apiUrl)
    let data = await response.json()
    data.forEach(d => {
        if (d.nom === getParams()) {
            displayInfos(d)
        }
    });

}


window.addEventListener('load', function() {
    getFunctions()
    backButton()
})