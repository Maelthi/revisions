const detailsUrl = '/details.html'
const apiUrl = 'https://api.myjson.com/bins/kgegi'

const router = () => {
    const cards = document.querySelectorAll('.card-container')
    cards.forEach(card => {
        card.addEventListener('click', function() {
            const functionCliqued = this.firstElementChild.id
            window.location = `${detailsUrl}?function=${functionCliqued}`
        })
    });
}

const getFunctions = async() => {
    let response = await fetch(apiUrl)
    let data = await response.json()
    return data
}

const displayEl = (data) => {
    data.forEach((func) => {
        const cardContainer = document.createElement('div')
        cardContainer.classList.add('card-container')
        cardContainer.innerHTML = `<h3 id="${func.nom}">${func.nom}</h3>
        <div class="type">${func.type}</div>
        <p class="description">${func.definition}</p>`
        const cardsContainer = document.getElementById('cards-container')
        cardsContainer.append(cardContainer)
    })
}

window.addEventListener('load', function() {
    getFunctions().then((data) => {
        displayEl(data)
        router()
    })
})
